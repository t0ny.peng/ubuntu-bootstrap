#!/usr/bin/env bash

RED='\033[0;31m'
NC='\033[0m' # No Color
BLUE="\033[1;34m"

set -eux
SCRIPT_PATH="$( cd "$(dirname "$0")" ; pwd -P )"
HOME_PATH="$(realpath ~)"

# Install from apt
sudo apt update
sudo apt install zsh autojump tmux curl -y

# Install oh my zsh
curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh > ${SCRIPT_PATH}/oh-my-zsh.sh
sudo chmod +x ${SCRIPT_PATH}/oh-my-zsh.sh
${SCRIPT_PATH}/oh-my-zsh.sh --unattended

echo "Please input the name of this machien that'll show up in ZSH"
read MACHINE_NAME
cp ${SCRIPT_PATH}/etc/robbyrussell.zsh-theme ${HOME_PATH}/.oh-my-zsh/themes
sed -i "s/HOSTNAME/${MACHINE_NAME}/g" ${HOME_PATH}/.oh-my-zsh/themes/robbyrussell.zsh-theme

# Oh my zsh plugin
git clone https://github.com/zsh-users/zsh-autosuggestions ~/.zsh/zsh-autosuggestions
echo "source ~/.zsh/zsh-autosuggestions/zsh-autosuggestions.zsh" >> ~/.zshrc

git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ~/.zsh/zsh-syntax-highlighting
echo "source ~/.zsh/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh" >> ~/.zshrc
echo ". /usr/share/autojump/autojump.sh" >> ~/.zshrc

cp ${SCRIPT_PATH}/etc/tmux.conf ${HOME_PATH}/.tmux.conf
